package application;

public class Fibonacci {

    public static void main(String[] args) {

        int element = 10;

        System.out.println(FibonacciMethods.fibonacciIteracyjnie(element));
        System.out.println(FibonacciMethods.fibonacciRekurencyjnie(element));
    }
}

class FibonacciMethods {

    public static int fibonacciRekurencyjnie(int n) {

        if (n < 2) {
            return n;
        }
        return fibonacciRekurencyjnie(n - 1) +
            fibonacciRekurencyjnie(n - 2);
    }

    public static int fibonacciIteracyjnie(int n) {

        int elementA = 0; // zmienne pomocnicze symbolizujące
        int elementB = 1; // element poprzedni B i jeszcze wcześniejszy A
        int wynik = 0; // zmienna wynik, pod którą podstawimy obliczoną wartość
        if (n < 2) {
            return n; // jeśli n<2 zwracamy n (dla zera 0 dla jedynki 1)
        }
        for (int i = 2; i <= n; i++) {
            wynik = elementA + elementB; // pod wynik podstawiamy sumę poprzednich elementów
            elementA = elementB; // modyfikujemy zmienne przechowujące
            elementB = wynik;  // dwie ostatnie wartości
        }
        return wynik; // zwracamy wynik
    }
}
