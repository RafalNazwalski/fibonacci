package application;

public class StringReverting {

    public static void main(String[] args) {

        String test = "Janusz";
        System.out.println(StringRevertingImpl.revert(test));
    }
}

class StringRevertingImpl {

    public static String revert(final String source) {

        String target = "";

        for (int i = 0; i < source.length(); i++) {
            target = target.concat(String.valueOf(source.charAt(source.length() - i - 1)));
        }

        return target;
    }
}
