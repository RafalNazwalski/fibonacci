package application.Java8;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Comparator {

    public static void main(String[] args) {

        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");

        Collections.sort(names, new java.util.Comparator<String>() {
            @Override
            public int compare(String a, String b) {
                return a.compareTo(b);
            }
        });

        names.forEach(System.out::println);
    }
}


