package application.Java8;

public class DefaultMethodForInterfaces {

    public static void main(String[] args) {

        FormulaImpl formulaImpl = new FormulaImpl();

        System.out.println(formulaImpl.calculate(10)); // 100.0
        System.out.println(formulaImpl.sqrt(16));           // 4.0

    }
}

interface Formula {

    double calculate(int a);

    default double sqrt(int a) {
        return Math.sqrt(a);
    }
}

class FormulaImpl implements Formula {

    @Override
    public double calculate(int a) {
        return sqrt(a * 100);
    }
}

