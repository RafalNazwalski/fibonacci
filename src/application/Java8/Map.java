package application.Java8;

import java.util.HashMap;

public class Map {

    public static void main(String[] args) {

        java.util.Map<Integer, String> map = new HashMap<>();

        for (int i = 0; i < 10; i++) {
            map.putIfAbsent(i, "value " + i);
        }

        System.out.println("First iteration:");
        map.forEach((id, val) -> System.out.println(val));

        for (int i = 8; i < 12; i++) {
            map.putIfAbsent(i, "value " + i);
        }

        System.out.println("Second iteration:");
        map.forEach((id, val) -> System.out.println(val));
    }
}



