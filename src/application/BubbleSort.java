package application;

import static application.BubbleSort.BubbleSortMethods.bubbleSort;

public class BubbleSort {

    public static void main(String[] args) {

        int arr[] = {3, 60, 35, 2, 45, 32, 5};

        System.out.println("Array Before Bubble Sort");

        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        bubbleSort(arr);//sorting array elements using bubble sort

        System.out.println("Array After Bubble Sort");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    static class BubbleSortMethods {

        public static void bubbleSort(int[] source) {

            int n = source.length;
            int temp = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 1; j < (n - i); j++) {
                    if (source[j - 1] > source[j]) {
                        //swap elements
                        temp = source[j - 1];
                        source[j - 1] = source[j];
                        source[j] = temp;
                    }

                }
            }
        }
    }
}
